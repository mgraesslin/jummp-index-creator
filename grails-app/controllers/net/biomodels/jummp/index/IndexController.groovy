package net.biomodels.jummp.index

import de.unirostock.configuration.Config
import de.unirostock.database.Manager
import de.unirostock.parser.SBML.SBMLExtractor
import de.unirostock.data.PersonWrapper
import de.unirostock.data.PublicationWrapper
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder

class IndexController {
    def remoteJummpApplicationAdapter
    def remoteModelAdapterDBusImpl

    def index() { }

    def createIndex() {
        Authentication auth = remoteJummpApplicationAdapter.authenticate(new UsernamePasswordAuthenticationToken(params.username, params.password))
        String path = params.path

        Config.dbPath = path
        Manager.instance()

        runAsync {
            SecurityContextHolder.context.authentication = auth
            Integer count = remoteModelAdapterDBusImpl.getModelCount()

            for (int i=0; i<count; i += 100) {
                def models = remoteModelAdapterDBusImpl.getAllModels(i, 100)
                models.each { model ->
                    byte[] data = remoteModelAdapterDBusImpl.retrieveModelFile(model.id)
                    PublicationWrapper publication = null
                    if (model.publication) {
                        publication = new PublicationWrapper(model.publication.title,
                                                      model.publication.journal,
                                                      model.publication.synopsis,
                                                      model.publication.affiliation,
                                                      "${model.publication.year}".toString(),
                                                      null)
                        model.publication.authors.each { author ->
                            publication.addAuthor(new PersonWrapper(author.firstName, author.lastName, null))
                        }
                    }
                    println "start extracting"
                    try {
                        SBMLExtractor.extractStoreIndex(data, publication, model.id)
                    } catch (Throwable e) {
                        println(e.message)
                        e.printStackTrace()
                    }
                    println("Model indexed: " + model.id)
                }
            }
        }
        render "Import started"
    }
}
